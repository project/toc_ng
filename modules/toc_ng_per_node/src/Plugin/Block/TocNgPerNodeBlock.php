<?php

namespace Drupal\toc_ng_per_node\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\toc_ng\Plugin\Block\TocNgBlock;

/**
 * Provides a 'Toc NG' per node block.
 *
 * @Block(
 *  id = "toc_ng_per_node_block",
 *  admin_label = @Translation("Toc NG per node block"),
 * )
 */
class TocNgPerNodeBlock extends TocNgBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'override_nodetype' => 1,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['override_nodetype'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override node type configuration'),
      '#description' => $this->t('Override the table of contents configuration defined in the node type.'),
      '#default_value' => $this->configuration['override_nodetype'],
    ];

    $form['custom_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom configuration'),
      '#description' => $this->t('Custom table of contents configuration specific to this block.'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="settings[override_nodetype]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $this->blockFormToc($form['custom_configuration'], 'custom_configuration');

    return $form;
  }

  /**
   * Configuration name array.
   *
   * @param string $name
   *   The name for the configuration item.
   *
   * @return string|array
   *   Array with the custom_configuration parent element name.
   */
  protected function getTocName(string $name): string|array {
    return ['custom_configuration', $name];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['override_nodetype'] = $form_state->getValue('override_nodetype');
    parent::blockSubmit($form, $form_state);
  }

  /**
   * Get the ToC NG configuration for a specific node type.
   *
   * @param \Drupal\node\NodeTypeInterface $type
   *   The node type to get the configuration from.
   *
   * @return array
   *   The node type ToC NG configuration array.
   */
  protected function getConfigurationFromNodeType(NodeTypeInterface $type):array {
    return [
      'toc_ng_active' => $type->getThirdPartySetting('toc_ng', 'toc_ng_active'),
      'title' => $type->getThirdPartySetting('toc_ng', 'title'),
      'title_tag' => $type->getThirdPartySetting('toc_ng', 'title_tag'),
      'title_classes' => $type->getThirdPartySetting('toc_ng', 'title_classes'),
      'selectors' => $type->getThirdPartySetting('toc_ng', 'selectors'),
      'selectors_minimum' => $type->getThirdPartySetting('toc_ng', 'selectors_minimum'),
      'container' => $type->getThirdPartySetting('toc_ng', 'container'),
      'list_type' => $type->getThirdPartySetting('toc_ng', 'list_type'),
      'prefix' => $type->getThirdPartySetting('toc_ng', 'prefix'),
      'classes' => $type->getThirdPartySetting('toc_ng', 'classes'),
      'back_to_top' => $type->getThirdPartySetting('toc_ng', 'back_to_top'),
      'back_to_top_label' => $type->getThirdPartySetting('toc_ng', 'back_to_top_label'),
      'heading_focus' => $type->getThirdPartySetting('toc_ng', 'heading_focus'),
      'back_to_toc' => $type->getThirdPartySetting('toc_ng', 'back_to_toc'),
      'back_to_toc_label' => $type->getThirdPartySetting('toc_ng', 'back_to_toc_label'),
      'back_to_toc_classes' => $type->getThirdPartySetting('toc_ng', 'back_to_toc_classes'),
      'smooth_scrolling' => $type->getThirdPartySetting('toc_ng', 'smooth_scrolling'),
      'highlight_on_scroll' => $type->getThirdPartySetting('toc_ng', 'highlight_on_scroll'),
      'highlight_offset' => $type->getThirdPartySetting('toc_ng', 'highlight_offset'),
      'sticky' => $type->getThirdPartySetting('toc_ng', 'sticky'),
      'sticky_offset' => $type->getThirdPartySetting('toc_ng', 'sticky_offset'),
      'ajax_page_updates' => $type->getThirdPartySetting('toc_ng', 'ajax_page_updates'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      /** @var \Drupal\node\NodeTypeInterface $node_type */
      $node_type = $node->__get('type')->entity;
      $toc_override = $node_type->getThirdPartySetting('toc_ng_per_node', 'override', 0);
      $toc_override_default = $node_type->getThirdPartySetting('toc_ng_per_node', 'override_default', 1);

      // Support toc_ng per-node feature.
      if ($node->hasField('toc_ng_active') && $toc_override) {
        // Use default override value if not set.
        if ($node->toc_ng_active->value == NULL) {
          $node->toc_ng_active->value = $toc_override_default;
        }
        if (empty($node->toc_ng_active->value)) {
          return [];
        }
      }

      $toc_ng_settings = $node_type->getThirdPartySettings('toc_ng');
      if (empty($toc_ng_settings['toc_ng_active'])) {
        // Toc has been disabled but the extra field hasn't been disabled.
        return [];
      }

      if ($this->configuration['override_nodetype']) {
        $toc_settings = $this->configuration;
        return $this->buildToc($toc_settings);
      }
      else {
        $toc_settings = $this->getConfigurationFromNodeType($node_type);
        return $this->buildToc($toc_settings);
      }

    }
    return parent::build();
  }

}
