<?php

namespace Drupal\toc_ng\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Toc NG' block.
 *
 * @Block(
 *  id = "toc_ng_block",
 *  admin_label = @Translation("Toc NG block"),
 * )
 */
class TocNgBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new SocialSimpleBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'title' => $this->t('Table of contents'),
      'title_tag' => 'div',
      'title_classes' => 'toc-title,h2',
      'selectors' => 'h2,h3',
      'selectors_minimum' => 0,
      'container' => '.node',
      'prefix' => 'toc',
      'list_type' => 'ul',
      'classes' => '',
      'back_to_top' => 0,
      'back_to_top_label' => $this->t('Back to top'),
      'heading_focus' => 0,
      'back_to_toc' => 0,
      'back_to_toc_label' => $this->t('Back to table of contents'),
      'back_to_toc_classes' => 'visually-hidden-focusable',
      'smooth_scrolling' => 1,
      'highlight_on_scroll' => 1,
      'highlight_offset' => 0,
      'sticky' => 0,
      'sticky_offset' => 0,
      'ajax_page_updates' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * Build a ToC NG configuration form with a parent element.
   *
   * @param array $form
   *   The form to build.
   * @param string|null $name
   *   The name for the parent element.
   */
  protected function blockFormToc(array &$form, string $name = NULL): void {

    $parent = empty($name) ? '' : '[' . $name . ']';

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The text to use as a title for the table of contents.'),
      '#default_value' => $this->configuration['title'],
    ];

    $form['title_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title HTML tag'),
      '#description' => $this->t('The HTML tag to use for the table of contents title (defaults to div).'),
      '#default_value' => $this->configuration['title_tag'],
    ];

    $form['title_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title CSS classes'),
      '#description' => $this->t('List of CSS classes to apply to the table of contents title tag (comma separated).'),
      '#default_value' => $this->configuration['title_classes'],
    ];

    $form['selectors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selectors'),
      '#description' => $this->t('Elements to use as headings. Each element separated by comma.'),
      '#default_value' => $this->configuration['selectors'],
    ];

    $form['selectors_minimum'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum elements'),
      '#description' => $this->t('Set a minimum of elements to display the toc. Set 0 to always display the TOC.'),
      '#default_value' => $this->configuration['selectors_minimum'],
    ];

    $form['container'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Container'),
      '#description' => $this->t('Element to find all selectors in.'),
      '#default_value' => $this->configuration['container'],
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('Prefix for anchor tags and class names.'),
      '#default_value' => $this->configuration['prefix'],
    ];

    $form['list_type'] = [
      '#type' => 'select',
      '#title' => $this->t('List type'),
      '#description' => $this->t('Select the list type to use.'),
      '#default_value' => $this->configuration['prefix'],
      '#options' => [
        'ul' => $this->t('Unordered HTML list (ul)'),
        'ol' => $this->t('Ordered HTML list (ol)'),
      ],
    ];

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Table of contents CSS classes'),
      '#description' => $this->t('List of CSS classes to apply to the table of contents DIV tag (comma separated).'),
      '#default_value' => $this->configuration['classes'],
    ];

    $form['back_to_top'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show "back to top" links'),
      '#description' => $this->t('Display "back to top" links next to headings.'),
      '#default_value' => $this->configuration['back_to_top'],
    ];

    $form['back_to_top_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Back to top link label'),
      '#description' => $this->t('The label to use for "back to top" links, span HTML tag is allowed.<br>WCAG: remember to define a visually-hidden/sr-only span label if you are using a CSS icon.'),
      '#default_value' => $this->configuration['back_to_top_label'] ?? 'Back to top',
      '#states' => [
        'visible' => [
          ':input[name="settings' . $parent . '[back_to_top]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['heading_focus'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Heading focus'),
      '#description' => $this->t('Set focus on corresponding heading when selected.'),
      '#default_value' => $this->configuration['heading_focus'],
    ];

    $form['back_to_toc'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show "back to toc" links'),
      '#description' => $this->t('Display "back to toc" links next to headings.'),
      '#default_value' => $this->configuration['back_to_toc'],
    ];

    $form['back_to_toc_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Back to toc link label'),
      '#description' => $this->t('The label to use for "back to toc" links, span HTML tag is allowed.<br>WCAG: remember to define a visually-hidden/sr-only span label if you are using a CSS icon.'),
      '#default_value' => $this->configuration['back_to_toc_label'],
      '#states' => [
        'visible' => [
          ':input[name="settings' . $parent . '[back_to_toc]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['back_to_toc_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Back to toc link CSS classes'),
      '#description' => $this->t('The CSS classes to add to the back to ToC link label (space separated).'),
      '#default_value' => $this->configuration['back_to_toc_classes'],
      '#states' => [
        'visible' => [
          ':input[name="settings' . $parent . '[back_to_toc]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['smooth_scrolling'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Smooth scrolling'),
      '#description' => $this->t('Enable or disable smooth scrolling on click.'),
      '#default_value' => $this->configuration['smooth_scrolling'],
    ];

    $form['highlight_on_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Highlight on scroll'),
      '#description' => $this->t('Add class to heading that is currently in focus.'),
      '#default_value' => $this->configuration['highlight_on_scroll'],
    ];

    $form['highlight_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Highlight offset'),
      '#description' => $this->t('Offset to trigger the next headline.'),
      '#default_value' => $this->configuration['highlight_offset'],
      '#states' => [
        'visible' => [
          ':input[name="settings' . $parent . '[highlight_on_scroll]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['sticky'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sticky'),
      '#description' => $this->t('Stick the toc on window scroll.'),
      '#default_value' => $this->configuration['sticky'],
    ];

    $form['sticky_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Sticky offset'),
      '#description' => $this->t('The offset (in px) to apply when the toc is sticky.'),
      '#default_value' => $this->configuration['sticky_offset'],
      '#states' => [
        'visible' => [
          ':input[name="settings' . $parent . '[sticky]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['ajax_page_updates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax page updates handling (Experimental)'),
      '#description' => $this->t('Update the ToC when the related containers are being replaced using Ajax.'),
      '#default_value' => $this->configuration['ajax_page_updates'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $this->blockFormToc($form);
    return $form;
  }

  /**
   * Get the ToC configuration name to use with form state.
   *
   * @param string $name
   *   The name of the configuration item.
   *
   * @return string|array
   *   The name to use with form state.
   */
  protected function getTocName(string $name): string|array {
    return $name;
  }

  /**
   * Process block ToC NG configuration submit.
   *
   * @param array $form
   *   The form to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to get the submitted values from.
   */
  protected function blockSubmitToc($form, FormStateInterface $form_state): void {
    $this->configuration['title'] = $form_state->getValue($this->getTocName('title'));
    $this->configuration['title_tag'] = $form_state->getValue($this->getTocName('title_tag'));
    $this->configuration['title_classes'] = $form_state->getValue($this->getTocName('title_classes'));
    $this->configuration['selectors'] = $form_state->getValue($this->getTocName('selectors'));
    $this->configuration['selectors_minimum'] = $form_state->getValue($this->getTocName('selectors_minimum'));
    $this->configuration['container'] = $form_state->getValue($this->getTocName('container'));
    $this->configuration['prefix'] = $form_state->getValue($this->getTocName('prefix'));
    $this->configuration['list_type'] = $form_state->getValue($this->getTocName('list_type'));
    $this->configuration['classes'] = $form_state->getValue($this->getTocName('classes'));
    $this->configuration['back_to_top'] = $form_state->getValue($this->getTocName('back_to_top'));
    $this->configuration['back_to_top_label'] = $form_state->getValue($this->getTocName('back_to_top_label'));
    $this->configuration['heading_focus'] = $form_state->getValue($this->getTocName('heading_focus'));
    $this->configuration['back_to_toc'] = $form_state->getValue($this->getTocName('back_to_toc'));
    $this->configuration['back_to_toc_label'] = $form_state->getValue($this->getTocName('back_to_toc_label'));
    $this->configuration['back_to_toc_classes'] = $form_state->getValue($this->getTocName('back_to_toc_classes'));
    $this->configuration['smooth_scrolling'] = $form_state->getValue($this->getTocName('smooth_scrolling'));
    $this->configuration['highlight_on_scroll'] = $form_state->getValue($this->getTocName('highlight_on_scroll'));
    $this->configuration['highlight_offset'] = $form_state->getValue($this->getTocName('highlight_offset'));
    $this->configuration['sticky'] = $form_state->getValue($this->getTocName('sticky'));
    $this->configuration['sticky_offset'] = $form_state->getValue($this->getTocName('sticky_offset'));
    $this->configuration['ajax_page_updates'] = $form_state->getValue($this->getTocName('ajax_page_updates'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->blockSubmitToc($form, $form_state);
  }

  /**
   * List of configuration items to add as attributes in the toc element.
   *
   * @return string[]
   *   The list of configuration item names to not add as attributes.
   */
  protected function getSettingsToIncludeAsAttributes(): array {
    return [
      'selectors',
      'selectors_minimum',
      'container',
      'prefix',
      'list_type',
      'back_to_top',
      'back_to_top_label',
      'heading_focus',
      'back_to_toc',
      'back_to_toc_label',
      'back_to_toc_classes',
      'smooth_scrolling',
      'highlight_on_scroll',
      'highlight_offset',
      'sticky',
      'sticky_offset',
      'ajax_page_updates',
    ];
  }

  /**
   * Build the render array for the toc element.
   *
   * @param array $settings
   *   The configuration of the toc to render.
   *
   * @return array
   *   The resulting render array.
   */
  protected function buildToc(array $settings) {
    $build = [];

    // Lambda function to clean css identifiers.
    $cleanCssIdentifier = function ($value) {
      return Html::cleanCssIdentifier(trim($value));
    };

    // toc-ng class is used to initialize the toc. Additional classes are added
    // from the configuration.
    $classes = array_map($cleanCssIdentifier, array_merge(['toc-ng'], explode(',', $settings['classes'])));
    $attributes = new Attribute(['class' => $classes]);
    $toc_id = Html::getUniqueId($cleanCssIdentifier($this->pluginId));
    $attributes->setAttribute('id', $toc_id);
    $title_id = $toc_id . '__title';
    $titleClasses = empty($settings['title_classes']) ? 'h2' : $settings['title_classes'];
    $titleClassesArray = array_map($cleanCssIdentifier, array_merge(['toc-title'], explode(',', $titleClasses)));
    $title_attributes = new Attribute([
      'id' => $title_id,
      'class' => $titleClassesArray,
    ]);

    if ($settings['sticky']) {
      $attributes->addClass('sticky');
    }

    $include_as_data_attributes = $this->getSettingsToIncludeAsAttributes();
    foreach ($settings as $name => $setting) {
      if (!in_array($name, $include_as_data_attributes)) {
        continue;
      }
      $data_name = 'data-' . $cleanCssIdentifier($name);
      switch ($name) {
        case 'back_to_top_label':
        case 'back_to_toc_label':
          $attributes->setAttribute($data_name, Xss::filter((string) $setting, ['span']));
          break;

        default:
          $attributes->setAttribute($data_name, Xss::filter((string) $setting, []));
          break;
      }
    }

    // Provide some entity context if available.
    $entity = NULL;
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      $entity = $node;
    }
    /** @var \Drupal\taxonomy\TermInterface $taxonomy_term */
    elseif ($taxonomy_term = $this->currentRouteMatch->getParameter('taxonomy_term')) {
      $entity = $taxonomy_term;
    }

    $build['toc_ng'] = [
      '#theme' => 'toc_ng',
      '#title' => Xss::filter($settings['title'], ['span']),
      '#tag' => Xss::filter($settings['title_tag'] ?: 'div', []),
      '#title_attributes' => $title_attributes,
      '#attributes' => $attributes,
      '#entity' => $entity,
      '#attached' => [
        'library' => [
          'toc_ng/toc',
        ],
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $toc_settings = $this->configuration;
    return $this->buildToc($toc_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    /** @var \Drupal\node\NodeInterface $node */
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), $node->getCacheTags());
    }
    /** @var \Drupal\taxonomy\TermInterface $taxonomy_term */
    elseif ($taxonomy_term = $this->currentRouteMatch->getParameter('taxonomy_term')) {
      return Cache::mergeTags(parent::getCacheTags(), $taxonomy_term->getCacheTags());
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
