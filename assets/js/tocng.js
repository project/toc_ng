/*!
 * Javascript code based on the original toc.js library.
 * Original code has been heavily modified and refactored.
 *
 * toc - jQuery Table of Contents Plugin
 * http://projects.jga.me/toc/
 * copyright Greg Allen 2014
 * MIT License
 */
(($) => {
  const PADDING_TOP_UPDATE_EVENT = 'toc_ng-padding-top-update';
  const TOC_UPDATE_EVENT = 'toc_ng-toc-update';
  const DATA_FOCUSED_ELEMENT_ID = 'focusedElementId';

  /**
   * The TocObject struct definition.
   * @typedef {Object} TocObject
   * @property {Object} opts - TODO.
   * @property {Object} element - TODO.
   * @property {Object} $element - TODO.
   * @property {Object} $container - TODO.
   * @property {Object} $headings - TODO.
   * @property {number} bodyPaddingTop - TODO.
   * @property {number|null} highlightTimeout - Highlight on scroll timeout
   * @property {number|null} scrollToTimeout - TODO.
   * @property {function} highlightOnScroll - TODO.
   * @property {function} scrollTo - TODO.
   * @property {function|undefined} scrollListener - TODO.
   * @property {boolean} updating - TODO.
   */

  /**
   * Get the new body padding-top and update the sticky positioning.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function refreshBodyPaddingTop(tocObj) {
    const bodyCompStyle = window.getComputedStyle(document.body);
    tocObj.bodyPaddingTop = parseInt(
      bodyCompStyle.getPropertyValue('padding-top'),
      10,
    );
    document.dispatchEvent(new Event(PADDING_TOP_UPDATE_EVENT));
  }

  /**
   * Handle body padding-top changes for correct sticky positioning.
   *
   * The Drupal admin toolbar adds a padding-top to the page body.
   * We try to handle it as properly as possible.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function initBodyPaddingTopObserver(tocObj) {
    const mutObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutationRecord) => {
        if (
          mutationRecord.type === 'attributes' &&
          mutationRecord.attributeName === 'style' &&
          mutationRecord.target === document.body
        ) {
          refreshBodyPaddingTop(tocObj);
        }
      });
    });
    mutObserver.observe(document.body, {
      attributes: true,
      attributeFilter: ['style'],
    });
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function initContainerObserver(tocObj) {
    const mutObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.type === 'childList') {
          const { removedNodes } = mutation;
          if (removedNodes.length) {
            removedNodes.forEach((node) => {
              tocObj.$container.each((i, container) => {
                if (node.contains(container)) {
                  document.dispatchEvent(new Event(TOC_UPDATE_EVENT));
                  return false;
                }
              });
            });
          }
        }
      });
    });
    mutObserver.observe(document, {
      childList: true,
      subtree: true,
    });
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function refreshHighlight(tocObj) {
    const scrollHeight = window.innerHeight - tocObj.bodyPaddingTop;
    let closest = Number.MAX_VALUE;
    let index = -1;
    for (let i = 0; i < tocObj.$headings.length; i += 1) {
      const headingRect = tocObj.$headings[i].getBoundingClientRect();
      const headingScrollTop = headingRect.top - tocObj.bodyPaddingTop;
      const currentClosest = Math.abs(headingScrollTop);
      if (currentClosest < closest) {
        closest = currentClosest;
        index = i;
        if (headingScrollTop > scrollHeight / 2) {
          // Go back to the previous index if not high enough on screen.
          index = i - 1;
        }
      } else {
        break;
      }
    }
    if (index >= 0) {
      $('li', tocObj.element).removeClass(tocObj.opts.activeClass);
      const $highlighted = $(`li:eq(${index})`, tocObj.element);
      $highlighted.addClass(tocObj.opts.activeClass);
      tocObj.opts.onHighlight($highlighted);
    }
  }

  function highlightOnScroll() {
    /** @type {TocObject} */
    const tocObj = this;
    if (tocObj.highlightTimeout) {
      clearTimeout(tocObj.highlightTimeout);
    }
    tocObj.highlightTimeout = setTimeout(() => {
      refreshHighlight(tocObj);
    }, 50);
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function addHighlightScrollListener(tocObj) {
    $(window).on(
      'scroll',
      ((to) => {
        to.scrollListener = () => {
          to.highlightOnScroll();
        };
        return to.scrollListener;
      })(tocObj),
    );
  }

  function scrollTo(e, anchor) {
    /** @type {TocObject} */
    const tocObj = this;
    if (tocObj.opts.smoothScrolling) {
      e.preventDefault();
      if (tocObj.opts.highlightOnScroll) {
        // If currently waiting for highlight re-enabling... Check below.
        if (tocObj.scrollToTimeout) {
          clearTimeout(tocObj.scrollToTimeout);
        }
        // Disable highlight before using scrollIntoView for performance reason.
        $(window).off('scroll', tocObj.scrollListener);
      }
      anchor.scrollIntoView({ behavior: 'smooth' });
      window.history.replaceState(undefined, undefined, `#${anchor.id}`);
      if (tocObj.opts.highlightOnScroll) {
        // Re-enable highlight after waiting for one second.
        tocObj.scrollToTimeout = setTimeout(() => {
          addHighlightScrollListener(tocObj);
        }, 1000);
      }
    }
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function processHeadings(tocObj) {
    const $tocEl = tocObj.$element;
    const { $headings, element, opts } = tocObj;

    let currentLevel = 0;
    let currentList;
    const $tocNav = $tocEl.find('nav');
    $tocNav.empty();
    let lastLi = $tocNav[0];

    $headings.each((i, heading) => {
      opts.generateHeadingId(heading, opts.prefix);

      // If specified, allow heading to take focus.
      if (opts.headingFocus) {
        heading.tabIndex = 0;
      }

      // build TOC item
      const a = document.createElement('a');
      a.textContent = opts.headerText(heading);
      a.href = `#${heading.id}`;
      a.addEventListener('click', (event) => {
        tocObj.scrollTo(event, heading);
        if (opts.headingFocus) {
          heading.focus({ preventScroll: true });
        }
        $('li', element).removeClass(opts.activeClass);
        $(event.target).parent().addClass(opts.activeClass);
        $tocEl.trigger('selected', event.target.href);
      });

      const li = document.createElement('li');
      li.classList.add(opts.itemClass(heading, opts.prefix));
      li.append(a);

      const level = heading.tagName.slice(-1);

      if (level === currentLevel) {
        currentList.appendChild(li);
        lastLi = li;
      } else if (level > currentLevel) {
        currentLevel = level;
        const ul = document.createElement(opts.listType);
        ul.level = level;
        ul.appendChild(li);
        lastLi.appendChild(ul);
        currentList = ul;
        lastLi = li;
      } else if (level < currentLevel) {
        while (level < currentLevel) {
          currentList = currentList.parentNode.parentNode;
          currentLevel = currentList.level;
        }
        currentList.appendChild(li);
        lastLi = li;
        currentLevel = level;
      }
    });
  }

  /**
   * Build "back to top" and "back to toc" links.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function buildTocAndTopLinks(tocObj) {
    const { element, $element, $headings, opts } = tocObj;
    const tocId = $element.attr('id');
    const firstHeaderId = $headings[0].id;
    const firstLinkSelector = `a[href="#${firstHeaderId}"]`;
    const $firstLink = $(firstLinkSelector, element);
    $element.find('a').each(function updateTocLinks(index) {
      const link = this;
      const linkHash = link.hash;
      const anchor = linkHash.replace(/^#/, '');
      const header = document.getElementById(anchor);
      if (opts.backToTop && index > 0) {
        const a = document.createElement('a');
        a.innerHTML = opts.backToTopLabel;
        a.href = `#${$headings[0].id}`;
        a.addEventListener('click', (e) => {
          const heading = document.getElementById($headings[0].id);
          tocObj.scrollTo(e, heading);
          if (opts.headingFocus) {
            heading.focus({ preventScroll: true });
          }
          $('li', element).removeClass(opts.activeClass);
          $firstLink.parent().addClass(opts.activeClass);
          $element.trigger('selected', linkHash);
        });
        $(a).addClass(opts.backToTopClass).insertBefore($(header));
      }
      if (opts.backToToc) {
        const anchorNameToToc = `${tocId}-${anchor}-to-toc`;
        $(link).attr('id', anchorNameToToc);
        const a = document.createElement('a');
        a.innerHTML = opts.backToTocLabel;
        a.href = `#${anchorNameToToc}`;
        a.addEventListener('click', (e) => {
          tocObj.scrollTo(e, link);
          link.focus({ preventScroll: true });
          $('li', element).removeClass(opts.activeClass);
          $(link).parent().addClass(opts.activeClass);
          $element.trigger('selected', linkHash);
        });
        $(a).addClass(opts.backToTocClasses).insertBefore($(header));
      }
    });
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   * @return {string} - The focused element id.
   */
  function getFocusedElementId(tocObj) {
    return tocObj.$element.data(DATA_FOCUSED_ELEMENT_ID);
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   * @param {string} id - The focused element id.
   */
  function setFocusedElementId(tocObj, id) {
    tocObj.$element.data(DATA_FOCUSED_ELEMENT_ID, id);
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function unsetFocusedElementId(tocObj) {
    tocObj.$element.data(DATA_FOCUSED_ELEMENT_ID, null);
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   * @return {Object} - The focusable element if found.
   */
  function getFirstFocusableElement(tocObj) {
    return tocObj.$element.find('button, a').first();
  }

  /**
   * Restore focus on previously focused element when toc is updated.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function restoreFocus(tocObj) {
    setTimeout(() => {
      // Get the ID of the previously focused element.
      const focusedElementId = getFocusedElementId(tocObj);
      if (focusedElementId) {
        // Unset the previously focused element.
        unsetFocusedElementId(tocObj);
        // Get element to focus using the stored focused element id.
        let focusedElement = tocObj.$element.find(`#${focusedElementId}`);
        if (focusedElement.length === 0) {
          // Use first focusable element if stored focused element not found.
          focusedElement = getFirstFocusableElement(tocObj);
        }
        if (focusedElement) {
          focusedElement.focus();
        }
      }
    }, 0);
  }

  function isFocusingOut(event, element) {
    return (
      event.relatedTarget === null || !element.contains(event.relatedTarget)
    );
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function manageFocus(tocObj) {
    // A11y: store the currently focused toc element to be able to restore the
    // focus on this element after a toc refresh caused by an Ajax page update.
    tocObj.$element
      .on('focusin', function focusIn(event) {
        // We store the currently focused element id in the toc.
        setFocusedElementId(tocObj, event.target.id);
      })
      .on('focusout', function focusOut(event) {
        if (!tocObj.updating && isFocusingOut(event, this)) {
          // Delete the stored focused element id when focus leaves the toc.
          unsetFocusedElementId(tocObj);
        }
      });
  }
  /**
   * Stick the toc.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function stickTheToc(tocObj) {
    const tocTop = tocObj.opts.stickyOffset + tocObj.bodyPaddingTop;
    tocObj.element.style.top = `${tocTop}px`;
  }

  /**
   * Build ToC.
   *
   * @param {TocObject} tocObj - The tocObj object.
   */
  function buildToc(tocObj) {
    tocObj.updating = true;
    processHeadings(tocObj);
    const tocLength = tocObj.$element.find('li').length;
    const minimum = tocObj.opts.selectorsMinimum;
    if (tocLength >= minimum) {
      if (
        (tocObj.opts.backToTop || tocObj.opts.backToToc) &&
        tocObj.$headings.length > 1
      ) {
        buildTocAndTopLinks(tocObj);
      }

      if (tocObj.$element.hasClass('sticky') && tocObj.opts.stickyOffset >= 0) {
        document.addEventListener(PADDING_TOP_UPDATE_EVENT, () => {
          stickTheToc(tocObj);
        });
        refreshBodyPaddingTop(tocObj);
      }

      tocObj.$element.show();
      tocObj.$container.find('a.back-to-top').show();
    }
    tocObj.updating = false;
  }

  /**
   * Build a TocObject instance for a specific ToC.
   *
   * @param {Object} element - TODO.
   * @param {Object} opts - TODO.
   * @return {TocObject} - The TocObject instance.
   */
  function initTocObject(element, opts) {
    const $container = $(opts.container);
    return {
      opts,
      element,
      $element: $(element),
      $container,
      $headings: $(opts.selectors, $container),
      highlightTimeout: null,
      scrollToTimeout: null,
      bodyPaddingTop: 0,
      highlightOnScroll,
      scrollTo,
      scrollListener: undefined,
      updating: false,
    };
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function updateTocObject(tocObj) {
    tocObj.$container = $(tocObj.opts.container);
    tocObj.$headings = $(tocObj.opts.selectors, tocObj.$container);
  }

  /**
   * @param {TocObject} tocObj - The tocObj object.
   */
  function addTocUpdateListener(tocObj) {
    document.addEventListener(TOC_UPDATE_EVENT, () => {
      updateTocObject(tocObj);
      buildToc(tocObj);
      restoreFocus(tocObj);
      if (tocObj.opts.highlightOnScroll) {
        refreshHighlight(tocObj);
      }
    });
  }

  $.fn.toc_ng = function fnTocNG(options) {
    const opts = $.extend({}, jQuery.fn.toc_ng.defaults, options);
    return this.each(function eachToc() {
      const tocObj = initTocObject(this, opts);
      initBodyPaddingTopObserver(tocObj);
      buildToc(tocObj);
      if (tocObj.opts.highlightOnScroll) {
        addHighlightScrollListener(tocObj);
        refreshHighlight(tocObj);
      }
      if (tocObj.opts.ajaxPageUpdates) {
        manageFocus(tocObj);
        initContainerObserver(tocObj);
        addTocUpdateListener(tocObj);
      }
    });
  };

  jQuery.fn.toc_ng.defaults = {
    container: 'body',
    selectorsMinimum: 0,
    listType: 'ul',
    selectors: 'h1,h2,h3',
    smoothScrolling: true,
    stickyOffset: 0,
    prefix: 'toc',
    activeClass: 'toc-active',
    onHighlight() {},
    highlightOnScroll: true,
    highlightOffset: 0,
    backToTop: false,
    headingFocus: false,
    backToToc: false,
    ajaxPageUpdates: false,
    generateHeadingId(heading, prefix) {
      if (heading.id.length) {
        // Use existing id if not empty.
        return heading.id;
      }
      // Source: https://www.30secondsofcode.org/js/s/remove-accents/
      let headingId = heading.textContent
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[^a-z0-9]/gi, ' ')
        .replace(/\s+/g, '-')
        .replace(/^-+|-+$/g, '');
      headingId = prefix ? `${prefix}-${headingId}` : headingId;
      // Handle potential headingId collision.
      if (document.getElementById(headingId)) {
        let newHeadingId;
        let j = 2;
        do {
          newHeadingId = `${headingId}-${j}`;
          j += 1;
        } while (document.getElementById(newHeadingId));
        headingId = newHeadingId;
      }
      // Define id to allow use as anchor.
      heading.id = headingId;
      return headingId;
    },
    headerText(heading) {
      return heading.textContent;
    },
    itemClass(heading, prefix) {
      const tagName = heading.tagName.toLowerCase();
      return prefix ? `${prefix}-${tagName}` : tagName;
    },
  };
})(jQuery);
