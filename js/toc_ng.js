/**
 * @file
 * Toc_ng.
 */
(($) => {
  Drupal.behaviors.toc_ng = {
    attach(context, settings) {
      once('toc_ng', '.toc-ng', context).forEach((elt) => {
        const $tocEl = $(elt);
        const options = {
          listType: $tocEl.data('list-type') === 'ol' ? 'ol' : 'ul',
          selectors: $tocEl.data('selectors') || 'h2, h3',
          container: $tocEl.data('container') || 'body',
          selectorsMinimum: $tocEl.data('selectors-minimum') || 0,
          stickyOffset: $tocEl.data('sticky-offset') || 0,
          prefix: $tocEl.data('prefix') || '',
          backToTop: !!$tocEl.data('back-to-top'),
          backToTopLabel: $tocEl.data('back-to-top-label') || 'Back to top',
          backToTopClass: 'back-to-top',
          headingFocus: !!$tocEl.data('heading-focus'),
          backToToc: !!$tocEl.data('back-to-toc'),
          backToTocLabel:
            $tocEl.data('back-to-toc-label') || 'Back to table of contents',
          backToTocClasses:
            $tocEl.data('back-to-toc-classes') || 'visually-hidden-focusable',
          highlightOnScroll: !!$tocEl.data('highlight-on-scroll'),
          highlightOffset: $tocEl.data('highlight-offset') || 0,
          smoothScrolling: !!$tocEl.data('smooth-scrolling'),
          ajaxPageUpdates: !!$tocEl.data('ajax-page-updates'),
        };
        $tocEl.toc_ng(options);
      });
    },
  };
})(jQuery);
